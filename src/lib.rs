use byteorder::WriteBytesExt;
pub use errors::{Error, Fd2mResult};
use macros::{ControllerInput, Rotator, Vector};
use safe_transmute::guarded_transmute_to_bytes_pod;
use std::{
    fs,
    io::{self, prelude::*},
    path::Path,
};

mod errors;
mod macros;

type BO = byteorder::LE;

/// Converts the framedump (.log) file at `framedump_path` to a macro
/// and writes it to `macro_writer`.
///
/// Uses fake physics frame timestamps for more consistent playback.
pub fn to_writer<FP, W>(framedump_path: FP, mut macro_writer: W) -> Fd2mResult
where
    FP: AsRef<Path>,
    W: Write,
{
    let framedump: Vec<rlframedump::Frame> =
        rlframedump::parse_file(framedump_path)?.collect::<Result<_, _>>()?;

    let steps: usize = framedump.iter().map(|f| f.run_frames).sum();

    let first_tick = framedump
        .iter()
        .find(|f| !f.steps.is_empty())
        .ok_or(Error::Empty)?
        .steps
        .first()
        .unwrap();

    let mut physics_time =
        (first_tick.physics_frame as f64 * (first_tick.timestep / 1000.0)) as f32;

    macro_writer.write_f32::<BO>(physics_time)?;
    macro_writer.write_f32::<BO>(0.0)?;
    macro_writer.write_u32::<BO>(steps as u32)?;

    for frame in framedump {
        let controller_input = ControllerInput::from(&frame);

        for step in &frame.steps {
            macro_writer.write_f32::<BO>(physics_time)?;

            let location_data = Vector::from(step.car_post_phys.location);
            //        let rotation_data = Rotator::from(frame.car_post_phys.rotation);
            let rotation_data = Rotator::default();
            let velocity_data = Vector::from(step.car_post_phys.linear_velocity);

            macro_writer.write_all(guarded_transmute_to_bytes_pod(&controller_input))?;
            macro_writer.write_all(guarded_transmute_to_bytes_pod(&location_data))?;
            macro_writer.write_all(guarded_transmute_to_bytes_pod(&rotation_data))?;
            macro_writer.write_all(guarded_transmute_to_bytes_pod(&velocity_data))?;

            physics_time += (step.timestep / 1000.0) as f32;
        }
    }

    macro_writer.flush()?;
    Ok(())
}

/// Converts the framedump (.log) file at `framedump_path` to a macro
/// and writes it to `macro_writer`.
///
/// Uses frame timestaps. Can result in inaccurate playback.
pub fn to_writer_old<FP, W>(framedump_path: FP, mut macro_writer: W) -> Fd2mResult
where
    FP: AsRef<Path>,
    W: Write,
{
    let framedump: Vec<rlframedump::Frame> =
        rlframedump::parse_file(framedump_path)?.collect::<Result<_, _>>()?;

    let start_time = framedump.first().ok_or(Error::Empty)?.time;

    macro_writer.write_f32::<BO>(start_time as f32)?;
    macro_writer.write_f32::<BO>(0.0)?;
    macro_writer.write_u32::<BO>(framedump.len() as u32)?;

    for frame in framedump {
        macro_writer.write_f32::<BO>(frame.time as f32)?;

        let location_data = Vector::from(frame.latest_step.car_post_phys.location);
        let rotation_data = Rotator::default();
        let velocity_data = Vector::from(frame.latest_step.car_post_phys.linear_velocity);
        let controller_input = ControllerInput::from(frame);

        macro_writer.write_all(guarded_transmute_to_bytes_pod(&controller_input))?;
        macro_writer.write_all(guarded_transmute_to_bytes_pod(&location_data))?;
        macro_writer.write_all(guarded_transmute_to_bytes_pod(&rotation_data))?;
        macro_writer.write_all(guarded_transmute_to_bytes_pod(&velocity_data))?;
    }

    macro_writer.flush()?;
    Ok(())
}

/// Converts the framedump (.log) file at `framedump_path` to a macro
/// and writes it to a file at `macro_path`.
pub fn to_file<FP, MP>(framedump_path: FP, macro_path: MP) -> Fd2mResult
where
    FP: AsRef<Path>,
    MP: AsRef<Path>,
{
    to_writer(
        framedump_path,
        io::BufWriter::new(fs::File::create(macro_path)?),
    )
}

/// Converts the framedump (.log) file at `framedump_path` to a macro
/// and writes it to a file at `macro_path`.
pub fn to_file_old<FP, MP>(framedump_path: FP, macro_path: MP) -> Fd2mResult
where
    FP: AsRef<Path>,
    MP: AsRef<Path>,
{
    to_writer_old(
        framedump_path,
        io::BufWriter::new(fs::File::create(macro_path)?),
    )
}

//#[cfg(test)]
//mod tests {
//    use super::*;
//    use byteorder::ReadBytesExt;
//    use macros::Rotator;
//    use safe_transmute::guarded_transmute_pod;
//    use std::mem::size_of;
//
//    #[repr(C)]
//    #[derive(Debug, Copy, Clone, Default)]
//    struct Frame {
//        time: f32,
//        input: ControllerInput,
//        location: Vector,
//        rotation: Rotator,
//        velocity: Vector,
//    }
//
//    #[derive(Debug, Clone)]
//    struct Macro {
//        start_time: f32,
//        end_time: f32,
//        length: u32,
//        frames: Vec<Frame>,
//    }
//
//    fn from_reader<R: Read>(mut macro_reader: R) -> Fd2mResult<Macro> {
//        let start_time = macro_reader.read_f32::<BO>()?;
//        let end_time = macro_reader.read_f32::<BO>()?;
//        let length = macro_reader.read_u32::<BO>()?;
//
//        let mut frames = Vec::new();
//
//        for _ in 0..length {
//            let time = macro_reader.read_f32::<BO>()?;
//
//            let mut input_bytes = [0u8; size_of::<ControllerInput>()];
//            let mut location_bytes = [0u8; size_of::<Vector>()];
//            let mut rotation_bytes = [0u8; size_of::<Rotator>()];
//            let mut velocity_bytes = [0u8; size_of::<Vector>()];
//            macro_reader.read_exact(&mut input_bytes)?;
//            macro_reader.read_exact(&mut location_bytes)?;
//            macro_reader.read_exact(&mut rotation_bytes)?;
//            macro_reader.read_exact(&mut velocity_bytes)?;
//            let input: ControllerInput = guarded_transmute_pod(&input_bytes).unwrap();
//            let location: Vector = guarded_transmute_pod(&location_bytes).unwrap();
//            let rotation: Rotator = guarded_transmute_pod(&rotation_bytes).unwrap();
//            let velocity: Vector = guarded_transmute_pod(&velocity_bytes).unwrap();
//
//            frames.push(Frame {
//                time,
//                input,
//                location,
//                rotation,
//                velocity,
//            });
//        }
//
//        Ok(Macro {
//            start_time,
//            end_time,
//            length,
//            frames,
//        })
//    }
//
//    fn from_file<P: AsRef<Path>>(macro_path: P) -> Fd2mResult<Macro> {
//        from_reader(io::BufReader::new(fs::File::open(macro_path)?))
//    }
//
//    #[test]
//    fn read_bakkes_macro() {
//        let _m = from_file("hcbmacro").unwrap();
//        write!(fs::File::create("hcbmacro.txt").unwrap(), "{:#?}", _m).unwrap();
//    }
//
//    #[test]
//    fn read_generated_macro() {
//        let _m = from_file("hcbmacro2").unwrap();
//        write!(fs::File::create("hcbmacro2.txt").unwrap(), "{:#?}", _m).unwrap();
//    }
//
//    #[test]
//    fn generate_macro() {
//        to_file("framedump.bin.log", "hcbmacro-gen").unwrap();
//        let _m = from_file("hcbmacro-gen").unwrap();
//        write!(fs::File::create("hcbmacro-gen.txt").unwrap(), "{:#?}", _m).unwrap();
//    }
//}
