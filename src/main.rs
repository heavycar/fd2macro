use failure::err_msg;
use lazy_static::lazy_static;
use log::{error, info};
use std::{
    env,
    fs::{self, DirEntry},
    io,
    path::PathBuf,
    process,
};
use structopt::StructOpt;

type FResult<T = ()> = Result<T, failure::Error>;

/// Convert a framedump to a macro.
#[derive(StructOpt)]
#[structopt(author = "")]
struct Opt {
    /// Path to framedump (.log) file to convert to macro
    ///
    /// If no path is specified, the most recently modified framedump in the log directory is used.
    framedump: Option<PathBuf>,
    /// Path to macro output file or directory
    ///
    /// Defaults to "hcbmacro" in the directory of the input.
    #[structopt(short = "o", long = "out")]
    out: Option<PathBuf>,
    /// Output directly to the macro path used by the game
    ///
    /// Path used is "%USERPROFILE%\Documents\My Games\Rocket League\TAGame\Logs\hcbmacro".
    ///
    /// The value of "out" is ignored if this is enabled.
    #[structopt(short = "d", long = "direct")]
    direct: bool,
    /// Use legacy output (use the framedumps's frame timestamps instead of separating physics steps
    /// into frames and calculating their timestamps).
    ///
    /// Can result in macros that don't closely match the original behaviour.
    #[structopt(short = "l", long = "legacy")]
    legacy: bool,
    /// Pass many times for more log output
    ///
    /// By default, it'll report info. Passing `-v` one time also prints
    /// debug, `-vv` enables trace.
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,
    /// Hide all log output except for errors
    #[structopt(short = "s", long = "silent")]
    silent: bool,
}

lazy_static! {
    static ref LOG_DIR: PathBuf = {
        PathBuf::from(env::var("USERPROFILE").expect("Failed to find home directory."))
            .join(r"Documents\My Games\Rocket League\TAGame\Logs")
    };
}

fn run() -> FResult {
    let opt: Opt = Opt::from_args();

    let log_level = if opt.silent {
        simplelog::LevelFilter::Error
    } else {
        match opt.verbose {
            0 => simplelog::LevelFilter::Info,
            1 => simplelog::LevelFilter::Debug,
            _ => simplelog::LevelFilter::Trace,
        }
    };
    simplelog::TermLogger::init(log_level, simplelog::Config::default())?;

    let framedump = match opt.framedump {
        Some(framedump) => framedump,
        None => {
            let mut log_entries: Vec<DirEntry> = fs::read_dir(&*LOG_DIR)?
                .filter_map(Result::ok)
                .filter(|f: &DirEntry| {
                    f.file_name()
                        .to_string_lossy()
                        .ends_with("framedump.bin.log")
                })
                .collect();
            log_entries.sort_unstable_by_key(|e| e.metadata().unwrap().modified().unwrap());
            log_entries
                .last()
                .ok_or_else(|| err_msg("No framedumps found in log directory."))?
                .path()
        }
    };

    let out_path = if opt.direct {
        LOG_DIR.join("hcbmacro")
    } else {
        match opt.out {
            Some(ref out) if out.is_dir() => out.join("hcbmacro"),
            Some(out) => out,
            None => framedump
                .parent()
                .ok_or_else(|| err_msg("Input path has no parent directory."))?
                .join("hcbmacro"),
        }
    };

    info!("Parsing {}", framedump.to_string_lossy());
    info!("Writing to {}", out_path.to_string_lossy());
    if !opt.legacy {
        fd2macro::to_file(&framedump, &out_path)?;
    } else {
        fd2macro::to_file_old(&framedump, &out_path)?;
    }
    info!("Done");
    Ok(())
}

fn main() {
    let res = match std::panic::catch_unwind(run) {
        Ok(r) => r,
        Err(e) => {
            error!("Panic: {:?}", e);
            process::exit(1);
        }
    };

    if let Err(e) = res {
        error!("{}", e);
        for cause in e.iter_causes() {
            error!("Cause:\n\t{}", cause);
        }
        let code = e
            .downcast_ref::<io::Error>()
            .and_then(io::Error::raw_os_error)
            .unwrap_or(1);
        process::exit(code);
    }
}
