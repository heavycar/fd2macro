use safe_transmute::PodTransmutable;

#[repr(C)]
#[derive(Debug, Copy, Clone, Default)]
pub struct Vector {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl From<rlframedump::Vec3> for Vector {
    fn from(v: rlframedump::Vec3) -> Vector {
        Vector {
            x: v.x as f32,
            y: v.y as f32,
            z: v.z as f32,
        }
    }
}

unsafe impl PodTransmutable for Vector {}

#[repr(C)]
#[derive(Debug, Copy, Clone, Default)]
pub struct Rotator {
    pub pitch: i32,
    pub yaw: i32,
    pub roll: i32,
}

impl From<rlframedump::Vec3> for Rotator {
    fn from(_: rlframedump::Vec3) -> Rotator {
        Default::default()
    }
}

unsafe impl PodTransmutable for Rotator {}

#[repr(C)]
#[derive(Debug, Copy, Clone, Default)]
pub struct ControllerInput {
    pub throttle: f32,
    pub steer: f32,
    pub pitch: f32,
    pub yaw: f32,
    pub roll: f32,
    pub dodge_forward: f32,
    pub dodge_strafe: f32,
    pub jump: bool,
    pub activate_boost: bool,
    pub holding_boost: bool,
    pub handbrake: bool,
    pub jumped: bool,
}

impl From<&rlframedump::Frame> for ControllerInput {
    fn from(frame: &rlframedump::Frame) -> ControllerInput {
        ControllerInput {
            throttle: frame.vt as f32,
            steer: frame.vs as f32,
            pitch: frame.vp as f32,
            yaw: frame.vy as f32,
            roll: frame.vr as f32,
            dodge_forward: frame.vdf as f32,
            dodge_strafe: frame.vds as f32,
            jump: frame.vj != 0,
            activate_boost: frame.vb != 0,
            holding_boost: frame.vb != 0,
            handbrake: frame.vhb != 0,
            jumped: frame.vj != 0,
        }
    }
}

impl From<rlframedump::Frame> for ControllerInput {
    fn from(frame: rlframedump::Frame) -> ControllerInput {
        ControllerInput::from(&frame)
    }
}

unsafe impl PodTransmutable for ControllerInput {}
