use failure::Fail;
use std::io;

pub type Fd2mResult<T = ()> = Result<T, Error>;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "Io error: {}", _0)]
    Io(io::Error),
    #[fail(display = "Framedump error: {}", _0)]
    Framedump(rlframedump::Error),
    #[fail(display = "Framedump had no inputs.")]
    Empty,
}

impl From<rlframedump::Error> for Error {
    fn from(e: rlframedump::Error) -> Error {
        Error::Framedump(e)
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::Io(e)
    }
}
