# fd2macro

```
fd2macro
Convert a framedump to a macro.

USAGE:
    fd2macro [FLAGS] [OPTIONS] [framedump]

FLAGS:
    -d, --direct     
            Output directly to the macro path used by the game
            
            Path used is "%USERPROFILE%\Documents\My Games\Rocket League\TAGame\Logs\hcbmacro".
            
            The value of "out" is ignored if this is enabled.
    -h, --help       
            Prints help information

    -s, --silent     
            Hide all log output except for errors

    -V, --version    
            Prints version information

    -v, --verbose    
            Pass many times for more log output
            
            By default, it'll report info. Passing `-v` one time also prints debug, `-vv` enables trace.

OPTIONS:
    -o, --out <out>    
            Path to macro output file or directory
            
            Defaults to "hcbmacro" in the directory of the input.

ARGS:
    <framedump>    
            Path to framedump (.log) file to convert to macro
            
            If no path is specified, the most recently modified framedump in the log directory is used.
```

## Building

```bash
cargo build --release
```