//struct ControllerInput {
//	float Throttle = .0f;
//	float Steer = .0f;
//	float Pitch = .0f;
//	float Yaw = .0f;
//	float Roll = .0f;
//	float DodgeForward = .0f;
//	float DodgeStrafe = .0f;
//	unsigned long Jump : 1;
//	unsigned long ActivateBoost : 1;
//	unsigned long HoldingBoost : 1;
//	unsigned long Handbrake : 1;
//	unsigned long Jumped : 1;
//};

struct ControllerInput {
	float Throttle = .0f;
	float Steer = .0f;
	float Pitch = .0f;
	float Yaw = .0f;
	float Roll = .0f;
	float DodgeForward = .0f;
	float DodgeStrafe = .0f;
	bool Jump = false;
	bool ActivateBoost = false;
	bool HoldingBoost = false;
	bool Handbrake = false;
	bool Jumped = false;
};
